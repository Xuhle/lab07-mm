﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections.Generic;

public class Lab02b_PlayerControlPrediction : NetworkBehaviour
{
    struct PlayerState
    {
        public int movementNumber;
        public float posX, posY, posZ;
        public float rotX, rotY, rotZ;
        public CharacterState animationState;
    }

    public enum CharacterState
    {
        Idle = 0,
        WalkingForward = 1,
        WalkingBackwards = 2,
        RunningForward = 3,
        RunningBackward = 4,
        Jumping = 5
    }

    [SyncVar(hook = "OnServerStateChanged")]
    PlayerState serverState;
    PlayerState predictaedState;

    Queue<KeyCode> pendingMoves;

    CharacterState characterAnimationState;

    public float playerSpeed;
    public Animator controller;

    void Start()
    {
        controller = GameObject.Find("UnityGuy").GetComponent<Animator>();
        InitState();
        predictaedState = serverState;

        if(isLocalPlayer)
        {
            pendingMoves = new Queue<KeyCode>();
            UpdatePredictedState();
        }

        SyncState();
    }

    void Update()
    {
        if (isLocalPlayer)
        {
            Debug.Log("Pending moves: " + pendingMoves.Count);

            KeyCode[] moveKeys = { KeyCode.A, KeyCode.S, KeyCode.D, KeyCode.W, KeyCode.Q, KeyCode.E, KeyCode.Space };

            bool somethingPressed = false;

            foreach (KeyCode moveKey in moveKeys)
            {
                if (!Input.GetKey(moveKey)) //If the currently observed key code is not pressed
                {
                    continue;               //Then do nothing!
                }

                somethingPressed = true;
                pendingMoves.Enqueue(moveKey);
                UpdatePredictedState();
                CmdMoveOnServer(moveKey);
            }

            if(!somethingPressed)
            {
                pendingMoves.Enqueue(KeyCode.Alpha0);
                UpdatePredictedState();
                CmdMoveOnServer(KeyCode.Alpha0);
            }
        }

        SyncState();
    }

    [Command]
    void CmdMoveOnServer(KeyCode pressedKey)
    {
        serverState = Move(serverState, pressedKey);
    }

    void InitState()
    {
        serverState = new PlayerState
        {
            movementNumber =0,
            posX = -119f,
            posY = 165.08f,
            posZ = -924f,
            rotX = 0f,
            rotY = 0f,
            rotZ = 0f
        };
    }

    void SyncState()
    {
        PlayerState stateToRender = isLocalPlayer ? predictaedState : serverState;

        transform.position = new Vector3(stateToRender.posX, stateToRender.posY, stateToRender.posZ);
        transform.rotation = Quaternion.Euler(stateToRender.rotX, stateToRender.rotY, stateToRender.rotZ);
        controller.SetInteger("CharacterState", (int)stateToRender.animationState);

    }

    PlayerState Move(PlayerState previous, KeyCode newKey)
    {
        float deltaX = 0, deltaY = 0, deltaZ = 0;
        float deltaRotationY = 0;

        switch (newKey)
        {
            case KeyCode.Q:
                deltaX = -playerSpeed;
                break;
            case KeyCode.S:
                deltaZ = -playerSpeed;
                break;
            case KeyCode.E:
                deltaX = playerSpeed;
                break;
            case KeyCode.W:
                deltaZ = playerSpeed;
                break;
            case KeyCode.A:
                deltaRotationY = -1f;
                break;
            case KeyCode.D:
                deltaRotationY = 1f;
                break;
            case KeyCode.Space:
                deltaY = 1f;
                break;
        }

        return new PlayerState
        {
            movementNumber = 1 + previous.movementNumber,
            posX = deltaX + previous.posX,
            posY = deltaY + previous.posY,
            posZ = deltaZ + previous.posZ,
            rotX = previous.rotX,
            rotY = deltaRotationY + previous.rotY,
            rotZ = previous.rotZ,
            animationState = CalcAnimation(deltaX, deltaY, deltaZ, deltaRotationY)
        };
    }

    void OnServerStateChanged(PlayerState newState)
    {
        serverState = newState;
        
        if(pendingMoves != null)
        {
            while(pendingMoves.Count>(predictaedState.movementNumber-serverState.movementNumber))
            {
                pendingMoves.Dequeue();
            }

            UpdatePredictedState();
        }
    }

    void UpdatePredictedState()
    {
        predictaedState = serverState;
        
        foreach(KeyCode movekey in pendingMoves)
        {
            predictaedState = Move(predictaedState, movekey);
        }
    }

    CharacterState CalcAnimation(float dX,float dY,float dZ,float dRY)
    {
        if(dX == 0 && dY==0 && dZ==0)
        {
            return CharacterState.Idle;
        }

        if (dX != 0 || dZ != 0)
        {
            if (dX > 0 || dZ > 0)
            {
                if (Input.GetKeyDown(KeyCode.LeftShift))
                {
                    return CharacterState.RunningForward;
                }
                else
                {
                    return CharacterState.WalkingForward;
                }
            }
            else
            {
                if (Input.GetKeyDown(KeyCode.LeftShift))
                {
                    return CharacterState.RunningBackward;
                }
                else
                {
                    return CharacterState.WalkingBackwards;
                }
            }
        }
        
        return CharacterState.Idle;
    }
}
